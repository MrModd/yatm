# Simple program that write "Hello World!"

# null symbol is ""
null_Symbol=

# The sintax is: currentState,readSymbol,nextState,writeSymbol,movement
a,,b,H,Right
b,,c,e,Right
c,,d,l,Right
d,,e,l,Right
e,,f,o,Right
f,,g,,Right
g,,h,W,Right
h,,i,o,Right
i,,j,r,Right
j,,k,l,Right
k,,l,d,Right
l,,m,!,Left
m,d,n,d,Left
n,l,o,l,Left
o,r,p,r,Left
p,o,q,o,Left
q,W,r,W,Left
r,,s,,Stop