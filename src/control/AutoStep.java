package control;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JOptionPane;

import view.GUI;

import exceptions.ProgramHaltedException;

/** Execute automatically the program loaded in the controller step by step
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class AutoStep extends java.lang.Thread {
	private int ms;
	private GUI gui;
	private static Lock lock = new ReentrantLock();
	
	/** Create an instance
	 * 
	 * @param milliseconds the interval between two steps
	 * @param g the window caller
	 */
	public AutoStep(int milliseconds, GUI g) {
		ms = milliseconds;
		gui = g;
	}
	
	public void run() {
		try {
			lock.lock();
			while(true) {
				Controller c = Controller.getController();
				c.nextStep();
				gui.refreshTape();
				AutoStep.sleep(ms);
			}
		} catch (ProgramHaltedException e) {
			gui.setStopped();
			JOptionPane.showMessageDialog(gui, "Program terminated.", "Program terminated", JOptionPane.INFORMATION_MESSAGE);
		} catch (InterruptedException e) {
			//Lets die
		} finally {
			lock.unlock();
		}
	}
	
	/** Change the interval value
	 * 
	 * @param milliseconds the new interval
	 */
	public void setMs(int milliseconds) {
		if (milliseconds > 0) ms = milliseconds;
	}
}
