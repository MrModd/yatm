package control;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import model.Instruction;
import model.InstructionSet;
import model.tape;
import view.GUI;
import exceptions.ProgramFileSyntaxException;
import exceptions.ProgramHaltedException;

/** The controller of the Turing Machine.
 * It implements the state, symbol and movement domains as <i>String</i>
 * Accepted movements are "Left", "Right" and "Stop"
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class Controller implements Serializable {
	private static final long serialVersionUID = 3300014192841954613L;
	private static boolean instanced = false;
	private static Controller controller;
	private tape<String> T;
	private String initialState;
	private String currentState;
	private Integer indexOfT;
	private Integer steps;
	private String nullSymbol;
	private InstructionSet<String, String> instructions;
	
	/** Because this class is a singleton this method should be called
	 * to get an instance of it.
	 * 
	 * @return the instanced object
	 */
	public static Controller getController() {
		if(!instanced) {
			controller = new Controller();
			instanced = true;
		}
		return controller;
	}
	
	/** Use the <i>getController()</i> method to instance this object
	 * 
	 */
	private Controller() {
		T = new tape<String>();
		currentState = null;
		indexOfT = 0;
		steps = 0;
		nullSymbol = null;
		instructions = new InstructionSet<String, String>();
	}
	
	/** Start the graphic interface
	 * 
	 */
	public void startGUI() {
		new GUI();
	}
	
	/** Read the program from a file
	 * 
	 * @param file the path of the file to read
	 * @throws IOException if something goes wrong with the reading of file
	 * @throws ProgramFileSyntaxException if the file does not contain a valid program
	 */
	public void loadProgram(String file) throws IOException, ProgramFileSyntaxException {
		BufferedReader fin = new BufferedReader(new FileReader(file));
		String line;
		line = fin.readLine();
		boolean setState = false;
		//Reset program
		this.clearProgram();
		//Read states
		while (line != null) {
			if (line.length() > 0 && !line.startsWith("#") && !line.startsWith("null_Symbol=")) {
				String[] state = line.split(",");
				if (state.length != 5) {
					this.clearProgram();
					throw new ProgramFileSyntaxException();
				}
				Instruction<String, String, String> i = new Instruction<String, String, String>(state[0], state[1], state[2], state[3], state[4]);
				instructions.add(i);
				if(!setState) {
					initialState = i.getCurrentState();
					currentState = i.getCurrentState();
					setState = true;
				}
			}
			else if (line.startsWith("null_Symbol=")) {
				nullSymbol = line.substring(12);
			}
			line = fin.readLine();
		}
		fin.close();
		if (nullSymbol == null || currentState == null) {
			this.clearProgram();
			throw new ProgramFileSyntaxException();
		}
	}
	
	/** Get the symbol that in the actual program represent the null symbol of the tape
	 * 
	 * @return the null symbol
	 */
	public String getNullSymbol() {
		return new String(nullSymbol);
	}
	
	/** Get how many steps are executed from the program starts
	 * 
	 * @return an <i>int</i> representing the number of steps
	 */
	public int getSteps() {
		return steps;
	}
	
	/** Get the position of the head on the tape
	 * 
	 * @return the position of the head (should be negative)
	 */
	public int getActualPosition() {
		return indexOfT;
	}
	
	/** Execute a single step according with the current state of the machine and the symbol on the tape
	 * 
	 * @throws ProgramHaltedException if the execution of the program reach the end
	 */
	@SuppressWarnings("unchecked")
	public void nextStep() throws ProgramHaltedException {
		//First step: symbol reading
		String s = T.get(indexOfT);
		if (s == null) s = nullSymbol;
		//Second step: get instruction
		Instruction<String, String, String> newInstruction = instructions.get(currentState, s);
		if(newInstruction == null) throw new ProgramHaltedException();
		//Third step: write new symbol
		if (!newInstruction.getNextSymbol().equals(nullSymbol)) T.set(indexOfT, newInstruction.getNextSymbol());
		else T.set(indexOfT, null);
		//Fourth step: set new state
		currentState = newInstruction.getNextState();
		//Fifth step: move the head
		if(newInstruction.getMovement().equals("Left")) indexOfT--;
		else if(newInstruction.getMovement().equals("Right")) indexOfT++;
		else if(newInstruction.getMovement().equals("Stop")) currentState = null;
		//Sixth step: increase the number of done steps
		steps++;
	}
	
	/*public String tapeSequence(int fromIndex, int toIndex) {
		String result = "";
		while (fromIndex <= toIndex) {
			if(T.get(fromIndex) != null) result = result + T.get(fromIndex++);
			else result = result + nullSymbol;
		}
		return result;
	}*/
	
	/** Get the symbol written on the specified position
	 * 
	 * @param index the position of the tape
	 * @return the symbol written in or the null symbol if that location isn't written
	 */
	public String getTapeSymbol(int index) {
		return (T.get(index) == null) ? nullSymbol : T.get(index);
	}
	
	/** Get a formatted array of <i>String</i> representing all the instruction of the program loaded
	 * 
	 * @return an array of <i>String</i>
	 */
	@SuppressWarnings("rawtypes")
	public String[] getProgram() {
		Instruction[] ins = instructions.toArray();
		String[] p = new String[ins.length];
		for (int i=0; i<ins.length; i++) {
			p[i] = "<" + ins[i].getCurrentState().toString() + ", " + ins[i].getCurrentSymbol().toString() + ", " + ins[i].getNextState().toString() + ", " + ins[i].getNextSymbol().toString() + ", " + ins[i].getMovement().toString() + ">\n";
		}
		return p;
	}
	
	/** Set a symbol on the selected position
	 * 
	 * @param index the position of the tape
	 * @param s the symbol to write
	 * @throws IllegalArgumentException if the position isn't valid
	 */
	public void setTape(int index, String s) throws IllegalArgumentException {
		T.set(index, s);
	}
	
	/** Delete all the symbol on the tape
	 * 
	 */
	public void clearTape() {
		T.erase();
		indexOfT = 0;
	}
	
	/** Return on the initial state of the program
	 * 
	 */
	public void rewindState() {
		currentState = initialState;
	}
	
	/** Reset the steps count
	 * 
	 */
	public void resetSteps() {
		steps = 0;
	}
	
	/** Delete all the instructions on the program set
	 * 
	 */
	public void clearProgram() {
		instructions.erase();
		currentState = null;
		nullSymbol = null;
		steps = 0;
	}
	
	/** Save the state of the controller on a file
	 * 
	 * @param file where to save the state
	 * @throws IOException if something goes wrong while saving
	 */
	public static void serialize(String file) throws IOException {
		FileOutputStream fout = new FileOutputStream(file);
		ObjectOutputStream obj = new ObjectOutputStream(fout);
		obj.writeObject(controller);
		obj.close();
	}
	
	/** Load the state of the controller from a file
	 * 
	 * @param file where to find the state
	 * @throws ClassNotFoundException if the file isn't valid
	 * @throws IOException if something goes wrong while reading the file
	 */
	public static void deserialize(String file) throws ClassNotFoundException, IOException {
		FileInputStream fin = new FileInputStream(file);
		ObjectInputStream obj = new ObjectInputStream(fin);
		controller = (Controller) obj.readObject();
		instanced = true;
	}
}
