package control;

import javax.swing.UIManager;

/** YATM (Yet Another Turing Machine) v1.0 by MrModd
 * Actual build: 20111001 1st, Oct 2011
 * First build: 20111001 1st, Oct 2011
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 * 
 * This program is free software: you can redistribute it and/or modify
 * 
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Federico "MrModd" Cosentino
 *
 */
public class EntryPoint {

	/** All begins here :)
	 * @param args not used
	 */
	public static void main(String[] args) {
		try {
			//Metal
			//String className = UIManager.getCrossPlatformLookAndFeelClassName();
			//System
			String className = UIManager.getSystemLookAndFeelClassName();
			//Motif
			//String className = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
			//GTK
			//String className = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
			UIManager.setLookAndFeel(className);
		} catch (Exception e) {
			//Do nothing
		}
		Controller controller = Controller.getController();
		controller.startGUI();
	}

}
