package exceptions;

/**
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class ProgramFileSyntaxException extends Exception {
	private static final long serialVersionUID = 1048302200757173367L;

	public ProgramFileSyntaxException() {
		super();
	}
}
