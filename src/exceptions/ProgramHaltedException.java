package exceptions;

/**
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class ProgramHaltedException extends Exception {
	private static final long serialVersionUID = -977725520237982665L;

	public ProgramHaltedException() {
		super();
	}
}
