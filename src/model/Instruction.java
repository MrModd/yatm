package model;

import java.io.Serializable;

/** The quintuple representing an instruction for the Turing Machine
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 * @param <STATE> States domain
 * @param <SYMBOL> Symbols domain
 * @param <MOVE> Moves domain
 */
public class Instruction<STATE, SYMBOL, MOVE> implements Serializable {
	private static final long serialVersionUID = -1614540757475106566L;
	public final STATE q;
	public final SYMBOL s;
	public final STATE nq;
	public final SYMBOL ns;
	public final MOVE m;
	
	/** Create the instruction
	 * 
	 * @param currentState
	 * @param currentSymbol
	 * @param nextState
	 * @param nextSymbol
	 * @param move
	 */
	public Instruction(STATE currentState, SYMBOL currentSymbol, STATE nextState, SYMBOL nextSymbol, MOVE move) {
		q = currentState;
		s = currentSymbol;
		nq = nextState;
		ns = nextSymbol;
		m = move;
	}
	
	public STATE getCurrentState() {
		return q;
	}
	
	public SYMBOL getCurrentSymbol() {
		return s;
	}
	
	public STATE getNextState() {
		return nq;
	}
	
	public SYMBOL getNextSymbol() {
		return ns;
	}
	
	public MOVE getMovement() {
		return m;
	}
}
