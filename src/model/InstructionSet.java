package model;

import java.io.Serializable;
import java.util.HashMap;

/** An HashMap containing all the instruction.
 * It is possible to extract an instruction by his current state and the read symbol
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 * @param <STATE> States domain
 * @param <SYMBOL> Symbols domain
 */
@SuppressWarnings("rawtypes")
public class InstructionSet<STATE, SYMBOL> implements Serializable {
	private static final long serialVersionUID = 4696151467266400878L;
	private HashMap<STATE, HashMap<SYMBOL, Instruction>> states;
	
	/** Create the set
	 * 
	 */
	public InstructionSet() {
		states = new HashMap<STATE, HashMap<SYMBOL, Instruction>>();
	}
	
	/** Add an instruction
	 * 
	 * @param i the instruction to add
	 */
	@SuppressWarnings("unchecked")
	public void add(Instruction i) {
		HashMap<SYMBOL, Instruction> h = states.get(i.getCurrentState());
		if (h == null) {
			HashMap<SYMBOL, Instruction> m = new HashMap<SYMBOL, Instruction>();
			m.put((SYMBOL) i.getCurrentSymbol(), i); //TODO check cast
			states.put((STATE) i.getCurrentState(), m);
		}
		else {
			h.put((SYMBOL) i.getCurrentSymbol(), i);
		}
	}
	
	/** Extract an instruction
	 * 
	 * @param q the current state of the instruction
	 * @param s the read symbol of the instruction
	 * @return the instruction if present or <b>null</b>
	 */
	public Instruction get(STATE q, SYMBOL s) {
		if(states.get(q) != null) return states.get(q).get(s);
		return null;
	}
	
	/** Remove an instruction
	 * 
	 * @param q the current state of the instruction
	 * @param s the read symbol of the instruction
	 * @return the instruction if present or <b>null</b>
	 */
	public Instruction remove(STATE q, SYMBOL s) {
		if(states.get(q) == null) return null;
		Instruction i = states.get(q).get(s);
		if (states.get(q).isEmpty()) states.remove(q);
		return i;
	}
	
	/** Determine the number of the instructions contained in this set
	 * 
	 * @return an <i>int</i> representing the dimension of the set
	 */
	@SuppressWarnings("unchecked")
	public int size() {
		STATE[] s = (STATE[]) states.keySet().toArray();
		int size = 0;
		for (int i=0; i<s.length; i++) {
			size += states.get(s[i]).size();
		}
		return size;
	}
	
	/** Extract all the instruction ad put them on an array
	 * 
	 * @return an array of <i>Instruction</i>
	 */
	@SuppressWarnings("unchecked")
	public Instruction[] toArray() {
		Instruction[] ins = new Instruction[this.size()];
		STATE[] s = (STATE[]) states.keySet().toArray();
		int index = 0;
		for (int i=0; i<s.length; i++) {
			SYMBOL[] s1 = (SYMBOL[]) states.get(s[i]).keySet().toArray();
			for (int j=0; j<s1.length; j++) {
				ins[index++] = states.get(s[i]).get(s1[j]);
			}
		}
		return ins;
	}
	
	/** Delete all the content of this set
	 * 
	 */
	public void erase() {
		states.clear(); //Garbage Collector should erase all contents of this  HashMap
	}
}
