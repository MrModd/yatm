package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/** A simple reimplementation of an array doubling with two direction movements
 * <b>null</b> represent the empty symbol of the alphabet
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class tape<T> implements Serializable {
	private static final long serialVersionUID = 3734679640364498861L;
	private List<T> Left;
	private List<T> Right;
	private T zero;
	
	/** Create a tape
	 * the initial dimension is 3 for <i>Left</i> and <i>Right</i> lists
	 */
	public tape() {
		this(3);
	}
	
	/** Create a tape
	 * 
	 * @param dim initial dimension for the <i>Left</i> and <i>Right</i> lists
	 */
	public tape(int dim) {
		Left = new ArrayList<T>(dim);
		Right = new ArrayList<T>(dim);
		zero = null;
	}

	/** Add an element to the Left list
	 * 
	 * @param e the element to add
	 * @return <b>true</b> if the element was added
	 */
	public boolean putLeft(T e) {
		return Left.add(e);
	}
	
	/** Add an element to the Right list
	 * 
	 * @param e the element to add
	 * @return <b>true</b> if the element was added
	 */
	public boolean putRight(T e) {
		return Right.add(e);
	}
	
	/**
	 * Set a symbol on the specified index of the tape.
	 * If the position is the last of the written tape and the symbol is <b>null</b>, then that
	 * element of the list is removed.
	 * 
	 * @param index could be positive or negative
	 * @param e symbol to write in at the specified position
	 * @throws IllegalArgumentException if index is far more than one step from the end of the written string
	 *                (the TM could not move more than one step from his current location)
	 */
	public void set(int index, T e) throws IllegalArgumentException{
		if (index < 0) {
			index = (index * -1)-1;
			if (Left.size() > index) {
				if (e == null && index == Left.size()-1) Left.remove(index);
				else Left.set(index, e);
			}
			else {
				if (index == Left.size()) Left.add(e);
				else if (index == Left.size()+1) { //If TM erases the last element on the left and then moves on the left it will be 2 elements far from the last one of the list,
					Left.add(null);                  //but it is legitimate
					Left.add(e);
				}
				else throw new IllegalArgumentException("Could not set symbol far more than one step from the end of the written tape");
			}
		}
		else if (index > 0) {
			index = index-1;
			if (Right.size() > index) {
				if (e == null && index == Right.size()-1) Right.remove(index);
				else Right.set(index, e);
			}
			else {
				if (index == Right.size()) Right.add(e);
				else if (index == Right.size()+1) { //If TM erases the last element on the right and then moves on the right it will be 2 elements far from the last one of the list,
					Right.add(null);                  //but it is legitimate
					Right.add(e);
				}
				else throw new IllegalArgumentException("Could not set symbol far more than one step from the end of the written tape");
			}
		}
		else zero = e;
	}
	
	/** Get the symbol at the <i>index</i> position
	 * 
	 * @param index could be positive or negative
	 * @return the selected symbol or <i>null</i> if there's no symbol written
	 */
	public T get(int index) {
		if(index == 0) return zero;
		if (index < 0 && Left.size() >= index*(-1)) return Left.get((index*(-1))-1);
		else if (index > 0 && Right.size() >= index) return Right.get(index-1);
		else return null;
	}
	
	/** Check if the tape contains some symbols
	 * 
	 * @return <b>true</b> if tape is empty
	 */
	public boolean isEmpty() {
		return Left.isEmpty() && Right.isEmpty() && zero == null;
	}
	
	/** Get the dimension of the written tape
	 * 
	 * @return an <b>int</b> representing the length of the tape
	 */
	public int size() {
		int size = Left.size() + Right.size();
		if(((Left.size() > 0 || Right.size() > 0) && !(Left.size() > 0 && Right.size() > 0) && zero != null) || (Left.size() > 0 && Right.size() > 0)) size++; //Should be ((Left.size() > 0 XOR Right.size() > 0) AND zero != null) OR (Left.size() > 0 AND Right.size() > 0)
		return size;
	}
	
	/** Get the dimension of the <i>Left</i> array
	 * 
	 * @return an <b>int</b> representing the length of the <i>Left</i> array
	 */
	public int sizeLeft() {
		return Left.size();
	}

	/** Get the dimension of the <i>Right</i> array
	 * 
	 * @return an <b>int</b> representing the length of the <i>Right</i> array
	 */
	public int sizeRight() {
		return Right.size();
	}
	
	/** Clear all the elements in the tape
	 * 
	 */
	public void erase() {
		Left.clear();
		Right.clear();
		zero = null;
	}
}
