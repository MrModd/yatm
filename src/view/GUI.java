package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;

import control.AutoStep;
import control.Controller;
import exceptions.ProgramFileSyntaxException;
import exceptions.ProgramHaltedException;

/** Write the main window of the program
 * 
 * Copyright (C) 2011  Federico "MrModd" Cosentino (http://mrmodd.it/)
 *
 */
public class GUI extends JFrame {
	private static final long serialVersionUID = 1560508750802569605L;
	JTextArea[] locations;
	JTextField numStep;
	JTextField tapePosition;
	JSlider slider;
	JTextField textSpeed;
	JButton startPause;
	JButton stop;
	JButton step;
	JTextArea program;
	JTextField inputTape;
	boolean inputTapeModified = false;
	
	AutoStep thread;
	
	/** Build the GUI when instanced
	 * 
	 */
	public GUI() {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		
		this.setTitle("YATM (Yet Another Turing Machine)");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowListener() {
			@Override
			public void windowActivated(WindowEvent arg0) { }
			@Override
			public void windowClosed(WindowEvent arg0) { }
			@Override
			public void windowClosing(WindowEvent arg0) {
				int input = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?", "Exiting", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				switch (input) {
				case 0 : {
					System.exit(0);
				}
				case 1 : {
					//Do nothing
				}
				}
			}
			@Override
			public void windowDeactivated(WindowEvent arg0) { }
			@Override
			public void windowDeiconified(WindowEvent arg0) { }
			@Override
			public void windowIconified(WindowEvent arg0) { }
			@Override
			public void windowOpened(WindowEvent arg0) { }
		});
		
		//Main panel
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		mainPanel.setLayout(new BorderLayout());
		
		//North (tape view)
		JPanel north = new JPanel();
		north.setBorder(BorderFactory.createTitledBorder(/*BorderFactory.createBevelBorder(BevelBorder.LOWERED)*/null, "Tape", TitledBorder.CENTER, TitledBorder.TOP));
		north.setLayout(new FlowLayout(FlowLayout.CENTER));
		JPanel northGrid = new JPanel();
		northGrid.setLayout(new GridLayout(1,17));
		locations = new JTextArea[17];
		for(int i=0; i<17; i++) {
			locations[i] = new JTextArea("");
			locations[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			locations[i].setPreferredSize(new Dimension(20,20));
			locations[i].setEditable(false);
			//TODO center text
			northGrid.add(locations[i]);
		}
		locations[8].setBorder(BorderFactory.createLineBorder(Color.RED));
		JButton clearTape = new JButton("Clear");
		JButton help = new JButton("Help");
		north.add(northGrid);
		north.add(clearTape);
		north.add(help);
		
		//Center (controls)
		JPanel center = new JPanel();
		center.setBorder(BorderFactory.createTitledBorder(/*BorderFactory.createBevelBorder(BevelBorder.LOWERED)*/null, "Controls", TitledBorder.CENTER, TitledBorder.TOP));
		center.setLayout(new BorderLayout());
		//First line
		JPanel centerNorth = new JPanel();
		centerNorth.setLayout(new FlowLayout(FlowLayout.CENTER));
		inputTape = new JTextField("Write on tape...");
		inputTape.setColumns(18);
		JButton writeTape = new JButton("Write");
		centerNorth.add(inputTape);
		centerNorth.add(writeTape);
		//Second and third line
		JPanel centerCenter = new JPanel();
		centerCenter.setLayout(new BorderLayout());
		JPanel centerCenterNorth = new JPanel();
		centerCenterNorth.setLayout(new FlowLayout(FlowLayout.CENTER));
		JLabel numStepText = new JLabel("Step:");
		numStep = new JTextField("0");
		numStep.setColumns(4);
		numStep.setEditable(false);
		JLabel tapePositionText = new JLabel("Tape position:");
		tapePosition = new JTextField("0");
		tapePosition.setColumns(4);
		tapePosition.setEditable(false);
		centerCenterNorth.add(numStepText);
		centerCenterNorth.add(numStep);
		centerCenterNorth.add(tapePositionText);
		centerCenterNorth.add(tapePosition);
		JPanel centerCenterCenter = new JPanel();
		centerCenterCenter.setLayout(new FlowLayout(FlowLayout.CENTER));
		JLabel speed = new JLabel("Speed");
		slider = new JSlider(25, 2025, 1025);
		slider.setPreferredSize(new Dimension(200, 60));
		slider.setMajorTickSpacing(1000);
		slider.setMinorTickSpacing(100);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		textSpeed = new JTextField("1025");
		textSpeed.setEditable(false);
		textSpeed.setColumns(3);
		JLabel millis = new JLabel("ms");
		centerCenterCenter.add(speed);
		centerCenterCenter.add(slider);
		centerCenterCenter.add(textSpeed);
		centerCenterCenter.add(millis);
		JPanel centerCenterSouth = new JPanel();
		centerCenterSouth.setLayout(new FlowLayout(FlowLayout.CENTER));
		startPause = new JButton("Start");
		stop = new JButton("Stop");
		stop.setEnabled(false);
		step = new JButton("Next Step");
		centerCenterSouth.add(startPause);
		centerCenterSouth.add(stop);
		centerCenterSouth.add(step);
		centerCenter.add(centerCenterNorth, BorderLayout.NORTH);
		centerCenter.add(centerCenterCenter, BorderLayout.CENTER);
		centerCenter.add(centerCenterSouth, BorderLayout.SOUTH);
		//Fourth line
		JPanel centerSouth = new JPanel();
		centerSouth.setLayout(new FlowLayout(FlowLayout.CENTER));
		JButton reset = new JButton("Reset");
		JButton loadConfig = new JButton("Load");
		JButton saveConfig = new JButton("Save");
		centerSouth.add(reset);
		centerSouth.add(loadConfig);
		centerSouth.add(saveConfig);
		center.add(centerNorth, BorderLayout.NORTH);
		center.add(centerCenter, BorderLayout.CENTER);
		center.add(centerSouth, BorderLayout.SOUTH);
		
		//East (program sequence)
		JPanel east = new JPanel();
		east.setBorder(BorderFactory.createTitledBorder(/*BorderFactory.createBevelBorder(BevelBorder.LOWERED)*/null, "Program", TitledBorder.CENTER, TitledBorder.TOP));
		east.setLayout(new BorderLayout());
		program = new JTextArea("Empty...");
		program.setEditable(false);
		JScrollPane scroll = new JScrollPane(program);
		scroll.setPreferredSize(new Dimension(150, 200));
		JButton loadProgram = new JButton("Load");
		JButton clearProgram = new JButton("Clear");
		east.add(scroll, BorderLayout.CENTER);
		JPanel eastSouth = new JPanel();
		eastSouth.setLayout(new FlowLayout(FlowLayout.CENTER));
		eastSouth.add(clearProgram);
		eastSouth.add(loadProgram);
		east.add(eastSouth, BorderLayout.SOUTH);
		
		//Adding panels
		mainPanel.add(north, BorderLayout.NORTH);
		mainPanel.add(center, BorderLayout.CENTER);
		mainPanel.add(east, BorderLayout.EAST);
		this.add(mainPanel);
		
		//Refreshing interfaces
		this.refreshTape();
		this.refreshProgram();
		
		//Set dimension and show window
		this.validate();
		this.pack(); //Set dimension of the window related on dimension of panels
		Dimension scrSize = toolkit.getScreenSize();
		int scrWidth = scrSize.width;
		int scrHeight = scrSize.height;
		this.setLocation((scrWidth/2)-(this.getWidth()/2), (scrHeight/2)-(this.getHeight()/2));
		this.setVisible(true);
		
		//ActionListeners
		help.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(GUI.this, "YATM (Yet Another Turing Machine)\nVersion 1.0 (20111001)\nCreated by MrModd (http://mrmodd.it/)\nFirst build: 1st, Oct 2011 (20111001)\n" +
																				"This program is under the GPLv3\nCheck the README for more help", "Credits", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		clearTape.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Controller c = Controller.getController();
				c.clearTape();
				GUI.this.refreshTape();
			}
		});
		inputTape.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (!inputTapeModified) {
					inputTape.setText("");
					inputTapeModified = true;
				}
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				//Do nothing
			}
		});
		writeTape.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (inputTape.getText().length() > 0) {
					Controller c = Controller.getController();
					c.clearTape();
					for (int i=0; i < inputTape.getText().length(); i++) {
						try {
							c.setTape(i, inputTape.getText(i, 1));
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(GUI.this, "There was an error writing tape:\n" + e.getMessage(), "Error: unable to write tape", JOptionPane.ERROR_MESSAGE);
						} catch (BadLocationException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(GUI.this, "There was an error writing tape.\nPlease retry.", "Error: unable to write tape", JOptionPane.ERROR_MESSAGE);
						}
					}
					GUI.this.refreshTape();
				}
				
			}
		});
		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				textSpeed.setText(((Integer)slider.getValue()).toString());
				if(thread != null) thread.setMs(slider.getValue());
			}
		});
		startPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(startPause.getText().equals("Start") || startPause.getText().equals("Continue")) {
					thread = new AutoStep(slider.getValue(), GUI.this);
					GUI.this.setStarted();
					thread.start();
				}
				else {
					if(thread != null) thread.interrupt();
					GUI.this.setPaused();
				}
			}
		});
		stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(thread != null) thread.interrupt();
				GUI.this.setStopped();
			}
		});
		step.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Controller c = Controller.getController();
				try {
					c.nextStep();
					GUI.this.refreshTape();
					GUI.this.setPaused();
				} catch (ProgramHaltedException e) {
					GUI.this.setStopped();
					JOptionPane.showMessageDialog(GUI.this, "Program terminated.", "Program terminated", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Controller c = Controller.getController();
				c.clearProgram();
				c.clearTape();
				GUI.this.refreshTape();
				GUI.this.refreshProgram();
				GUI.this.setStopped();
				slider.setValue(1025);
				inputTape.setText("Write on tape...");
				inputTapeModified = false;
				if(thread != null) thread.interrupt();
			}
		});
		loadConfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GUI.this.loadController();
				GUI.this.refreshTape();
				GUI.this.refreshProgram();
				Controller c = Controller.getController();
				if (c.getSteps() > 0) GUI.this.setPaused();
			}
		});
		saveConfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GUI.this.saveController();
			}
		});
		clearProgram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Controller c = Controller.getController();
				c.clearProgram();
				GUI.this.refreshTape();
				GUI.this.refreshProgram();
				GUI.this.setStopped();
			}
		});
		loadProgram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GUI.this.loadProgram();
				GUI.this.refreshTape();
				GUI.this.refreshProgram();
			}
		});
	}
	
	/** Configure buttons visibility for a started execution
	 * 
	 */
	public void setStarted() {
		startPause.setText("Pause");
		stop.setEnabled(true);
		step.setEnabled(false);
	}
	
	/** Configure buttons visibility for a paused execution
	 * 
	 */
	private void setPaused() {
		startPause.setText("Continue");
		stop.setEnabled(true);
		step.setEnabled(true);
	}
	
	/** Configure buttons visibilty for a stopped execution
	 * and reset the state on the controller
	 */
	public void setStopped() {
		Controller c = Controller.getController();
		c.rewindState();
		c.resetSteps();
		startPause.setText("Start");
		stop.setEnabled(false);
		step.setEnabled(true);
	}
	
	/** Refresh symbols of the tape shown on the GUI
	 * 
	 */
	public void refreshTape() {
		Controller c = Controller.getController();
		int index = c.getActualPosition();
		for (int i=0; i<17; i++) {
			locations[i].setText(c.getTapeSymbol(index-8+i));
		}
		numStep.setText(((Integer)c.getSteps()).toString());
		tapePosition.setText(((Integer)c.getActualPosition()).toString());
	}
	
	/** Refresh the list of instructions
	 * 
	 */
	private void refreshProgram() {
		Controller c = Controller.getController();
		program.setText("");
		String[] p = c.getProgram();
		for (int i=0; i<p.length; i++) {
			program.append(p[i]);
		}
		if(program.getText().equals("")) program.setText("Empty...");
		program.setCaretPosition(0);
	}
	
	/** Save the current state
	 * 
	 */
	private void saveController() {
		try {
			JFileChooser j = new JFileChooser();
			j.setFileSelectionMode(JFileChooser.FILES_ONLY);
			j.setMultiSelectionEnabled(false);
			j.setCurrentDirectory(new File(System.getProperty("user.home")));
			j.setFileFilter(new FileNameExtensionFilter("YATM config file", "yatm"));
			if (j.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				String path = j.getSelectedFile().getPath();
				if (!path.endsWith(".yatm")) path = path + ".yatm";
				Controller.serialize(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "There was an error saving configuration.\nTry another path and retry.", "Error: unable to save configuration", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/** Load the current state
	 * 
	 */
	private void loadController() {
		try {
			JFileChooser j = new JFileChooser();
			j.setFileSelectionMode(JFileChooser.FILES_ONLY);
			j.setMultiSelectionEnabled(false);
			j.setCurrentDirectory(new File(System.getProperty("user.home")));
			j.setFileFilter(new FileNameExtensionFilter("YATM config file", "yatm"));
			if (j.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				Controller.deserialize(j.getSelectedFile().getPath());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "The selected file doesn't contain a valid configuration.\nPlease use a correct file.", "Error: file not valid", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "There was an error loading configuration file.\nTry another path and retry.", "Error: unable to load configuration", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/** Load the program
	 * 
	 */
	private void loadProgram() {
		try {
			JFileChooser j = new JFileChooser();
			j.setFileSelectionMode(JFileChooser.FILES_ONLY);
			j.setMultiSelectionEnabled(false);
			j.setCurrentDirectory(new File(System.getProperty("user.home")));
			j.setFileFilter(new FileNameExtensionFilter("Turing Machine program file", "tm"));
			if (j.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				Controller c = Controller.getController();
				c.loadProgram(j.getSelectedFile().getPath());
			}
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "There was an error loading the file.\nTry another path and retry.", "Error: unable to load the file", JOptionPane.ERROR_MESSAGE);
		} catch (ProgramFileSyntaxException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Incorrect file sintax.\nPlease select a correct file and retry.", "Error: wrong file sintax", JOptionPane.ERROR_MESSAGE);
		}
	}
}
